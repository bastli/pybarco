PyBarco shall contain a python interface with potentially a GUI, 
an option to select and mix effects and a preview/simulator that allows for development of effects even when not connected to the barco strips. 

Furthermore, the project shall contain a small library to generate and manipulate the matrices used to control the LEDs. 
Those Matrices shall have the form N x 112 X 3, where N denotes the amount of barco strips used. It is sorted from the largest to the smallest unit.  
The other values then corresponds to the 112 LEDs per strip and RGB values for each LED.

Currently the project contains a simulator prototype by Jonas and a small library by Sven to contain everythin in one place. 
Theese are included for reference but shall be integrated if possible into the project.
