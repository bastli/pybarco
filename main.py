#    _____         .__        
#   /     \ _____  |__| ____  
#  /  \ /  \\__  \ |  |/    \ 
# /    Y    \/ __ \|  |   |  \
# \____|__  (____  /__|___|  /
#         \/     \/        \/ 

"""This file handels the hole process including the GUI, processing and uploading"""

__author__ = 'Manumerous'
__maintainer__ = 'Manumerous'
__created__ = '36c3, December 2019'
__license__ = 'GNU General Public License'

import manipulators
from net_interface import NetInterface

IP = '151.217.142.197'
PORT = 1337    

def main():
    
    barco = NetInterface(IP, PORT)
    barco.clear_strips()

    current_matrix = manipulators.strip_command_matrix(30,30,0)
    print(current_matrix)

    barco.command_matrix_to_command_list(current_matrix)
   
if __name__ == "__main__":
    main()