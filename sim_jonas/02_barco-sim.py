import socket
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
# from numba import jit


UDP_IP = 'localhost'
UDP_PORT = 1337
udp_cnt = 0

N_LED = 112
N_STR = 15

def led_sim(val):
    if val <= 10:
        return 0
    else:
        # return int(0.75 * val + 63)
        return val


sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))
print('LISTENING ON %{}:%{}'.format(UDP_IP, UDP_PORT))

plt.ion()
fig, ax = plt.subplots()
x, y, col = [],[], []
ax.set_facecolor('black')
ax.spines['top'].set_color('grey')
ax.spines['bottom'].set_color('grey')
ax.tick_params(axis='y', colors='grey')

fig.set_facecolor('black')
sc = ax.scatter(x,y,c=col)
plt.xlim(-1,N_STR*2) # N_STR*2 to get a smaller gap between
plt.ylim(0,N_LED-1)

# global array for x-posiotion of points
for i in range(N_STR):
    x.extend([i]*N_LED)
# global array for y-posiotion of points
for i in range(N_STR):
    y.extend(range(N_LED,0,-1))
# global array for color of points
for i in range(N_LED*N_STR):
    col.append('#000000')
    i += 1


def fetch_led_data(udp_pkg):
        stripe = udp_pkg[0]
        leds = list()
        i = 1
        while i <= 3*N_LED-2:
            leds.append('#{:02x}{:02x}{:02x}'.format(led_sim(udp_pkg[i]),led_sim(udp_pkg[i+1]),led_sim(udp_pkg[i+2])))
            i += 3
        col[stripe*N_LED:(stripe+1)*N_LED] = leds

def draw():
    sc.set_offsets(np.c_[x,y])
    sc.set_facecolors(col)
    fig.canvas.draw()
    plt.pause(0.001)


# /////  main loop  /////
while True:
    udp_cnt = 0
    while udp_cnt < N_STR:  # wait for a new packet for each stripe before drawing
        data, addr = sock.recvfrom(1024)
        udp_cnt += 1
        # print('MESSAGE RECEIVED FROM %{}:\n%{}\n\n'.format(addr, data))
        fetch_led_data(data)

    draw()

# plt.waitforbuttonpress()
